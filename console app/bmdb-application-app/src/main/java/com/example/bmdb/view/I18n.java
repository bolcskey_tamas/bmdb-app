package com.example.bmdb.view;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class I18n {

	@Value("hu_HU")
    private Locale locale;

    private MessageSource messageSource;
    
    public I18n()
    {
    	ResourceBundleMessageSource messages =  new ResourceBundleMessageSource();
		messages.addBasenames("messages/messages");
		messages.setDefaultEncoding("UTF-8");
		messageSource = messages;
    }

	public String getMessage(String code, Object... args)
	{
		return messageSource.getMessage(code, args, locale);
	}
}
