package com.example.bmdb.app;

import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.example.bmdb.config.AppConfig;

public class App {

	private final Logger logger = LoggerFactory.getLogger(App.class);
	private Review review;
	private Media selectedMedia;
	private User currentUser;
	@Autowired
	private Service service;
	@Autowired
	private View view;

	public void Play(){
		logger.info("play");
		service.createTestData();
		createUser();
		view.printWelcomeMessage(currentUser);
		doReview();
	}
	
	private void createUser() {
		currentUser = view.readUserData();
		if(service.findUser(currentUser.getName()) == null)
			service.saveUser(currentUser);
		else
			currentUser = service.findUser(currentUser.getName());
		logger.info("User created: " + currentUser.getName());
	}
	
	private void doReview(){
		logger.info("User (" + currentUser.getName() + ") is doing review");
		do {
			view.printMedias(service.findAllMedia());
			selectedMedia = service.findMedia(view.readId());
			review = new Review.Builder()
					.setCreator(currentUser)
					.setMedia(selectedMedia)
					.setText(view.readReviewText())
					.setRating(view.readRating())
					.Build();
			service.saveReview(review);
			printReviewAverage();
		} while (view.readDone());
		logger.info("User (" + currentUser.getName() + ") stopped doing reviews");
	}
	
	private void printReviewAverage(){
		logger.info("printReviewAverage");
		double average = service.findMedia(selectedMedia.getId())
				.getReviews()
				.stream()
				.map(x->Rating.valueOfRating(x.getRating()))
				.mapToDouble(x->x)
				.average().orElse(0);
		view.printAverage(average);
	}


	public static void main(String[] args) {
        try (ConfigurableApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class)) {
            App app = appContext.getBean(App.class);
            app.Play();
        }
    }
		
}


	
	
	
	
	

