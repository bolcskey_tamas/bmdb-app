package com.example.bmdb.view;

import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.bmdb.domain.Actor;
import com.example.bmdb.domain.Media;
import com.example.bmdb.domain.Rating;
import com.example.bmdb.domain.Review;
import com.example.bmdb.domain.User;

public class View {

	static Scanner input = new Scanner(System.in);
	@Autowired
	I18n internationalizer;
	
	public View()
	{
		
	}

	public User readUserData() {

		System.out.println(internationalizer.getMessage("userName"));
		String name = input.nextLine();
		System.out.println(internationalizer.getMessage("userEmail"));
		String email = input.nextLine();
		System.out.println(internationalizer.getMessage("userPassword"));
		String password = input.nextLine();
		return new User.Builder().setName(name).setEmail(email).setPassWord(password).Build();
	}

	public void printWelcomeMessage(User user) {
		//System.out.println("Welcome to the movie and series review application, " + user.getName() + "!");
		System.out.println(internationalizer.getMessage("welcomeMessage",user.getName()));
		
	}

	public void printMedias(List<Media> medias) {
		StringBuilder sb = new StringBuilder();
		for(Media media : medias) {
			sb.append(internationalizer.getMessage("id",media.getId()))
			.append(internationalizer.getMessage("title",media.getTitle()))
			.append(internationalizer.getMessage("description",media.getDescription()))
			.append(internationalizer.getMessage("premier",media.getPremier()))
			.append(internationalizer.getMessage("cast"));
			for(Actor actor : media.getActors()) {
				sb.append(internationalizer.getMessage("actor",actor.getName(),actor.getBorn(),actor.getBiography()));
			}
			sb.append(internationalizer.getMessage("reviews"));
			for(Review review: media.getReviews()) {
				sb.append(internationalizer.getMessage("review",review.getText(),review.getCreator().getName(),review.getRating()));
			}
		}
		System.out.println(sb.toString());
	}

	public void printReview(User user) {
		StringBuilder sb = new StringBuilder();
		for (Review review : user.getReviews()) {
			sb.append(review.toString() + "\n");
		}
		System.out.println(sb.toString());
	}

	public long readId() {
		System.out.println(internationalizer.getMessage("readId"));
		long id = Long.parseLong(input.nextLine());
		return id;
	}

	public String readReviewText() {
		System.out.println(internationalizer.getMessage("readReviewText"));
		String text = input.nextLine();
		return text;
	}

	public Rating readRating() {
		System.out.println(internationalizer.getMessage("readRating"));
		Rating rating = Rating.valueOf(input.nextLine().toUpperCase());
		return rating;
	}

	public Boolean readDone() {
		System.out.println(internationalizer.getMessage("readDone",internationalizer.getMessage("yes"),internationalizer.getMessage("no")));
		String choice = input.nextLine();
		return choice.equalsIgnoreCase(internationalizer.getMessage("yes"));
	}
	
	public void printAverage(double average)
	{
		System.out.println(internationalizer.getMessage("printAverage",average));
	}
}
