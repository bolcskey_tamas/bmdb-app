package com.example.bmdb.config;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.example.bmdb.app.App;
import com.example.bmdb.service.Service;
import com.example.bmdb.view.View;

@Configuration
@Import({SpringConfigurationDataSource.class, SpringConfigurationJpa.class})
@ComponentScan("com.example")
public class AppConfig {

	@Bean
	public App createApp()
	{
		return new App();
	}

	@Bean
	public View createView() {
		return new View();
	}

	@Bean
	public Service createService() {
		return new Service();
	}
}
