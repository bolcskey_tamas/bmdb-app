package com.example.bmdb.domain;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Review {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToOne
	private Media media;

	private String text;

	@OneToOne
	private User creator;

	private Rating rating;

	public Review()
	{
		
	}
	
	private Review(Builder builder) {
		super();
		this.media = builder.media;
		this.text = builder.text;
		this.creator = builder.creator;
		this.rating = builder.rating;
	}

	public Media getMedia() {
		return media;
	}

	public String getText() {
		return text;
	}

	public User getCreator() {
		return creator;
	}

	public Rating getRating() {
		return rating;
	}

	@Override
	public String toString() {
		return "Review [text=" + text + ", creator=" + creator.getName() + ", rating=" + rating + "]";
	}

	public static class Builder {
		private Media media;

		private String text;

		private User creator;

		private Rating rating;

		public Builder setMedia(Media media) {
			this.media = media;
			return this;
		}

		public Builder setText(String text) {
			this.text = text;
			return this;
		}

		public Builder setCreator(User creator) {
			this.creator = creator;
			return this;
		}

		public Builder setRating(Rating rating) {
			this.rating = rating;
			return this;
		}

		public Review Build() {
			return new Review(this);
		}

	}
}
