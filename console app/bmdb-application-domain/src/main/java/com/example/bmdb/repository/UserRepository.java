package com.example.bmdb.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.bmdb.domain.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByName(String name);
}
