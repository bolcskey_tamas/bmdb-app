package com.example.bmdb.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.bmdb.domain.Series;


public interface SeriesRepository extends CrudRepository<Series, Long> {

}
