package com.example.bmdb.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;
import com.example.bmdb.web.model.UserLoginRequest;

@Controller
@SessionAttributes("User")
public class ValidationController {

	@Autowired
	private Service service;
	
	@RequestMapping(value = "/validation.html")
	public String Validate(@ModelAttribute("User")User user)
	{
		if (user!=null) {
			return "forward:home.html";
		} else {
			return "redirect:/";
		}
	}
	
	@ModelAttribute("User")
	public User getUser(UserLoginRequest userLoginRequest) {
		User user = service.isValidUser(userLoginRequest.getEmail(), userLoginRequest.getPassword());
		return user;
	}
	
	

}
