package com.example.bmdb.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.bmdb.web.model.UserLoginRequest;



@Controller
public class LoginController {
	
	@RequestMapping(value = "/")
	public String Login()
	{
		return "login";
	}		
	
	@ModelAttribute("userLoginRequest")
	public UserLoginRequest createUserLoginRequest() {
		return new UserLoginRequest();
	}
	
	

}
