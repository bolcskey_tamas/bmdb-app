package com.example.bmdb.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.example.bmdb.domain.User;
import com.example.bmdb.service.Service;

@Controller
public class HomeController {

	@Autowired
	private Service service;
	@RequestMapping(value = "/home.html")
	private String Home()  {
		return "home";
	}
	
	@RequestMapping(value = "/saveUser")
	public String SaveUser(@ModelAttribute("UserToEdit")User user)
	{
		service.saveUser(user);
		return "home";
	}
	
	@RequestMapping(value = "/logout")
	public String LogOut(HttpServletRequest request)
	{
		request.getSession().invalidate();
		return "redirect:/";
	}
	
	@ModelAttribute("UserToEdit")
	public User GetUser(@SessionAttribute("User")User user) {
		return user;
		
	}
	
	

}
