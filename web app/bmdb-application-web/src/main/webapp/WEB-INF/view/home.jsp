<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/style.css">

<title>Home</title>
</head>

<body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">MediaReview</a>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link disabled" href="#">Media</a>
				</li>
				<li class="nav-item"><a class="nav-link disabled" href="#">Language</a>
				</li>
			</ul>
			
			<a href="/logout" class="btn btn-dark btn-outline-light">Logout</a>
		</div>
	</nav>

	<div class="container pt-4">
		<div class=row>
			<div class=col-sm>
				<div class="card">
					<div class="card-header">User details</div>
					<form:form modelAttribute="UserToEdit" action="/saveUser"
						enctype="multipart/form-data">
						<div class="card-body">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroup-sizing-default">Name</span>
								</div>
								<form:input path="name" id="input_name" class="form-control"
									aria-label="Default"
									aria-describedby="inputGroup-sizing-default"
									placeholder="${UserToEdit.name}" />
							</div>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="inputGroup-sizing-default">Email
										address</span>
								</div>
								<form:input path="email" id="input_email" type="text"
									class="form-control" aria-label="Default"
									aria-describedby="inputGroup-sizing-default"
									placeholder="${UserToEdit.email}" />
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form:form>
				</div>
			</div>
		</div>
		<div class="row pt-4">
			<div class=col-sm>
				<div class="card">
					<div class="card-header">Reviews</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Review text</th>
									<th scope="col">Creator</th>
									<th scope="col">Associated media</th>
									<th scope="col">Rating</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="review" items="${UserToEdit.reviews}" varStatus="loop">
									<tr>
										<th scope="row">${loop.count}</th>
										<td><c:out value="${review.text}" /></td>
										<td><c:out value="${review.creator}" /></td>
										<td><c:out value="${review.media}" /></td>
										<td><c:out value="${review.rating}" /></td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>




</body>

</html>