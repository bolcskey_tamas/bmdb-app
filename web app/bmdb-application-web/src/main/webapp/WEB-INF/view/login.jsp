<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
    <head>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">

        <title>Login</title>
    </head>

    <body>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <div class=jumbotron>
            <h1>Welcome to BMDB!</h1>
            <h6>A media rating interface for reviewing and checking out details of series' and movies.</h6>
        </div>

        <div class="container">
            <p id=p1>
                <a href="">Login</a> or <a href="">Register</a>
            </p>
            <div class=row >
                <div class=col-sm-3 >
                    <div class="card">
                        <div class="card-header">
                            Login
                        </div>
                        <div class="card-body">
                            <form:form modelAttribute="userLoginRequest" action="validation.html"
				enctype="multipart/form-data">
                                <div class="form-group">
                                    <form:input type="email" path="email" id="input_email" class="form-control" placeholder="Email"/>
                                </div>
                                <div class="form-group">
                                    <form:input type="password" path="password" id="input_password" class="form-control" placeholder="Password"/>
                                </div>
                                <button type="submit" class="btn btn-primary">Login</button>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    </body>

</html>