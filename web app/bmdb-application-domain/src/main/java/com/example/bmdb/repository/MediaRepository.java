package com.example.bmdb.repository;


import org.springframework.data.repository.CrudRepository;

import com.example.bmdb.domain.Media;

public interface MediaRepository extends CrudRepository<Media, Long> {

	Media findById(long id);
}
