package com.example.bmdb.repository;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;

import com.example.bmdb.domain.Movie;

public interface MovieRepository extends CrudRepository<Movie, BigDecimal> {

}
